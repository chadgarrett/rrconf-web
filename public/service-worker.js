var CACHE_NAME = 'rrconf-v4';

var CACHE_FILES = [
    'scripts/install.js',
    'scripts/luxon-1.11.4.js',

    'images/favicon.ico',
    'images/install.svg',
    'images/icons/icon-144x144.png',
    'images/icons/icon-192x192.png',
    'images/icons/icon-512x512.png',

    'styles/inline.css',

    'index.html'
];

self.addEventListener('install', (e) => {
    console.log('[Service Worker] Install');

    e.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            console.log('[Service Worker] Caching all: app shell and content');
            return cache.addAll(CACHE_FILES);
        })
    );
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        // Cache first approch 
        caches.open(CACHE_NAME).then(function (cache) {
            return cache.match(event.request).then(function (response) {
                return response || fetch(event.request).then(function (response) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            });
        })
    );
});